###################
Purpose
###################

This web application created by Paulus Kristian to fulfill requirement test for Backend Engineer at Fabelio.com


*******************
Dependencies
*******************

Bootstrap 4 (https://getbootstrap.com/)

Sweet alert 2 (https://sweetalert2.github.io/)

CanvasJS (https://canvasjs.com/)


************
Deployment
************

1. Change base url in application/config/config.php

2. Update database settings in application/config/database.php

3. Deploy database struture in sql/fabelio.sql

4. Setup cronjob to schedule price retrieval.
	Controller Name : Welcome.php
	Function Name : update_price

5. Setup cronjob to executed price retrieval schedule.
	Controller Name : Welcome.php
	Function Name : do_scheduler
