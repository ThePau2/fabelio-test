
<?php

class All_model extends CI_Model {

    public function _construct()
    {
        parent::_construct();
    }

    public function base64_url_encode($input)
    {
        return rtrim(strtr(base64_encode($input), '+/', '-_'), '=');
    }

    public function base64_url_decode($input)
    {
        return base64_decode(str_pad(strtr($input, '-_', '+/'), strlen($input) % 4, '=', STR_PAD_RIGHT));
    }

    public function encode_val($val)
    {
        $token = substr(sha1(rand()), 0, 12);
        $token2 = substr(sha1(rand()), 0, 18);

        return $this->base64_url_encode($token.$val.$token2);
    }

    public function decode_val($val)
    {
        $val2 = $this->base64_url_decode($val);
        $tkn1 = substr($val2,0,12);
        $tkn2 = substr($val2,-18);

        $return = $val2;
        $return = str_replace($tkn1, "", $return);
        $return = str_replace($tkn2, "", $return);

        return $return;
    }


    function select_template($field, $field_value, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field, $field_value);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }


    function select_template_row($field, $field_value, $table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field, $field_value);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    function insert_template($data_fields, $table)
    {
        $this->db->insert($table, $data_fields);
    }

    function update_template($data_fields, $field, $field_id, $table)
    {
        $this->db->where($field, $field_id);
        $this->db->update($table, $data_fields);
    }

    

    public function get_price_history($product_id,$data_count='0')
    {
        $sql = "SELECT * FROM fab_price_history WHERE product_id = $product_id and deleted_at IS NULL ORDER BY created_at ASC";
        if($data_count){
            $sql .= " LIMIT $data_count,1";
        }
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function get_price_high_low($product_id)
    {
        $sql = "SELECT MIN(new_price) AS lowest,MAX(new_price) AS highest FROM fab_price_history WHERE product_id = $product_id";
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    public function get_product($product_id)
    {
        $sql = "SELECT * FROM fab_product WHERE product_id = $product_id and deleted_at IS NULL LIMIT 1";
        $query = $this->db->query($sql);

        if($query->num_rows()>0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    public function save_product($link,$product_id,$product_title,$product_price,$product_desc,$json,$save_history='0'){
        $data_fields['product_id'] = $product_id;
        $data_fields['product_title'] = $json['product']['name'];
        $data_fields['product_desc'] = $product_desc;
        $data_fields['product_price'] = $new_price = $json['product']['unit_sale_price'];
        $data_fields['product_img'] = $json['product']['product_image_url'];
        $data_fields['product_link'] = $link;
        $data_fields['product_json'] = json_encode($json);
        $product = $this->get_product($product_id);

        if($product){
            $data_fields['created_at'] = date("Y-m-d H:i:s");
            $this->update_template($data_fields,'product_id',$product_id,'fab_product');

            $old_price = $product->product_price;
        }else{
            $data_fields['created_at'] = date("Y-m-d H:i:s");
            $data_fields['updated_at'] = date("Y-m-d H:i:s");
            $this->insert_template($data_fields,'fab_product');

            $old_price = 0;$save_history=1;
        }

        if($save_history){
            $this->save_price_history($product_id,$new_price,$old_price);
        }
    }

    public function save_price_history($product_id,$new_price,$old_price){
        $data_fields['product_id'] = $product_id;
        $data_fields['new_price'] = $new_price;
        $data_fields['old_price'] = $old_price;
        $data_fields['created_at'] = date("Y-m-d H:i:s");
        $data_fields['updated_at'] = date("Y-m-d H:i:s");

        $this->insert_template($data_fields,'fab_price_history');
    }


    public function create_scheduler($product_id,$product_link,$product_price)
    {
        $data_fields['product_id'] = $product_id;
        $data_fields['product_link'] = $product_link;
        $data_fields['product_price'] = $product_price;
        $data_fields['created_at'] = date("Y-m-d H:i:s");

        $this->insert_template($data_fields,'fab_retrieve_scheduler');
        
    }

    public function finish_scheduler($scheduler_id,$product_json,$product_price)
    {
        $data_fields['json_result'] = json_encode($product_json);
        $data_fields['price_result'] = $product_price;
        $data_fields['finished_at'] = date("Y-m-d H:i:s");
        $this->update_template($data_fields,'id',$scheduler_id,'fab_retrieve_scheduler');
    }



}
?>