<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Fabelio Product List Page</title>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/simple-sidebar.css'); ?>" rel="stylesheet">
</head>
<body>

<style>
  .prod_img{
    max-width: 100px;
  }
  .container-fluid{
    padding:40px 20px;
  }
</style>
<div class="d-flex" id="wrapper">

  <!-- Page Content -->
  <div id="page-content-wrapper">
    <?php include("inc_topbar.php");?>

    <div class="container-fluid">
      <h1 class="mt-4">Product List</h1>
      <!-- <button class="btn btn-primary" id="sync_coordinate" style="float: right;">Sync Coordinates</button> -->

      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Image</th>
                <th width="300">Product Name</th>
                <th width="150">Price</th>
                <th>Description</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
          <?php 
            if($products):
              foreach($products as $p):
          ?>
	            <tr>
                <td><img src="<?php echo $p->product_img;?>" class="prod_img"/></td>
                <td><?php echo $p->product_title;?></td>
                <td>Rp <?php echo number_format($p->product_price, 0, ",", ".");?></td>
                <td><?php echo $p->product_desc;?></td>
                <td style="text-align:center">
                  <a class="btn btn-primary" href="<?php echo base_url(); ?>product_detail/<?php echo $this->All_model->encode_val($p->product_id);?>">Details</a>
                </td>
             </tr>
          <?php
              endforeach;
            endif;
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

</script>

<?php include("inc_footer.php");?>