<style>
	.list-group-item{
		border:none;
	}
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
  <div>Fabelio Price Monitor</div>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
    	<li>
    		<a href="<?php echo base_url();?>" class="list-group-item list-group-item-action bg-light">Submission Page</a>
    	</li>
    	<li>
  			<a href="<?php echo base_url();?>product_list" class="list-group-item list-group-item-action bg-light">Product Page</a>
  		</li>
    </ul>
  </div>
</nav>