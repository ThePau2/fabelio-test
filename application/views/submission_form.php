<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Fabelio Submission Page</title>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/simple-sidebar.css'); ?>" rel="stylesheet">
</head>
<body>
<div class="d-flex" id="wrapper">


  <!-- Page Content -->
  <div id="page-content-wrapper">
    <?php include("inc_topbar.php");?>

    <div class="container-fluid">
      <h1 class="mt-4">Submission Page</h1>
      <form method="POST" action="<?php echo base_url();?>do_submission">
        <div class="row">
          <div class="col-6">
            Please input Fabelio product page link
            <input type="text" name="link" class="form-control" placeholder="Fabelio Link" />
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="col-6">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- /#page-content-wrapper -->

</div>



<div id="add_modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Candidate</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?php echo base_url();?>candidates/add">
          <div class="row">
            <div class="col-sm-12">
              <input type="text" name="candidate_name" class="candidate_name form-control" required placeholder="Candidate Name"/>
            </div>
          </div>
          <br/>
          <div class="row">
            <div class="col-sm-6">
              <input type="number" name="candidate_post_code" class="candidate_post_code form-control" required placeholder="Post Code"/>
            </div>
            <div class="col-sm-6">
              <input type="number" name="travelling_treshold" class="travelling_treshold form-control" required placeholder="Travelling Threshold (Min)"/>
            </div>
          </div>
          <br/><br/>
          <div class="row">
            <div class="col-sm-12">
              <button type="Submit" class="btn btn-primary">Add</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="edit_modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Candidate</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?php echo base_url();?>candidates/update">
          <input type="hidden" name="candidate_id" class="candidate_id form-control" required />
          <div class="row">
            <div class="col-sm-12">
              <input type="text" name="candidate_name" class="candidate_name form-control" required placeholder="Candidate Name"/>
            </div>
          </div>
          <br/>
          <div class="row">
            <div class="col-sm-6">
              <input type="number" name="candidate_post_code" class="candidate_post_code form-control" required placeholder="Post Code"/>
            </div>
            <div class="col-sm-6">
              <input type="number" name="travelling_treshold" class="travelling_treshold form-control" required placeholder="Travelling Threshold (Min)"/>
            </div>
          </div>
          <br/><br/>
          <div class="row">
            <div class="col-sm-12">
              <button type="Submit" class="btn btn-primary">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/sweetalert_2.8.js'); ?>?<?php echo uniqid();?>"></script>

<script>

submission_error = "<?php echo $this->session->flashdata('submission_error'); ?>";

if(submission_error){
  Swal.fire(
    submission_error,
    '',
    'error'
  );
}


candidate_success = "<?php echo $this->session->flashdata('candidate_success'); ?>";

if(candidate_success){
  Swal.fire(
    candidate_success,
    '',
    'success'
  );
}


jQuery(document).ready(function($) {
  $("#add_candidate").click(function(event) {
    $("#add_modal").modal('show');
  });

  $(".btn_edit").click(function(){
      uuid = $(this).attr('uuid');
      $.ajax({
        url : "<?php echo site_url('candidates/get_candidate_ajax')?>",
        type: "POST",
        dataType: "JSON",
        data: {candidate_id:uuid},
        success: function (data) {
        }
      }).done(function(data) {
        if(data.status=="success"){
          $("#edit_modal .candidate_name").val(data.candidate_name);
          $("#edit_modal .candidate_post_code").val(data.candidate_post_code);
          $("#edit_modal .travelling_treshold").val(data.travelling_treshold);
          $("#edit_modal .candidate_id").val(uuid);
          $("#edit_modal").modal('show');
        }else{
          Swal.fire(
            'Candidate not found',
            '',
            'error'
          );
        }
      });
      
  });


  $(".btn_delete").click(function(){
      uuid = $(this).attr('uuid');
      Swal.fire({
        title: 'Are you sure you want to delete this candidate?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.value) {
          window.location.href="<?php echo base_url(); ?>candidates/delete/"+uuid;
        }
      });
      
  });
});
</script>
<?php include("inc_footer.php");?>