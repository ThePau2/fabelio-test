<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Fabelio Product Detail Page</title>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/simple-sidebar.css'); ?>" rel="stylesheet">
</head>
<body>

<style>
  .container-fluid{
    padding:40px 20px;
  }
  .prod_img{
    max-width: 100%;
    border:1px solid #dedede;
  }

  #chartContainer{
    margin:40px 20px;
  }

  h3.chart_title{
    margin: 40px;
    text-align: center;
  }

  .minmax{
    font-weight: bold;
    font-size:16px;
    margin:0 20px;
  }

  .highest{
    color:red;
  }

  .lowest{
    color:green;
  }
</style>
<div class="d-flex" id="wrapper">

  <!-- Page Content -->
  <div id="page-content-wrapper">
    <?php include("inc_topbar.php");?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 mb-2">
          <img src="<?php echo $product->product_img;?>" class="prod_img"/>
        </div>
        <div class="col-md-6 mb-2">
          <h1 class=""><?php echo $product->product_title;?></h1>
          <h3 class="">Rp <span class="current_price"><?php echo number_format($product->product_price, 0, ",", ".");?></span></h3>
          <br/>
          <div class="desc">
            <?php echo $product->product_desc;?>
          </div>
        </div>
      </div>
      <h3 class="chart_title">Price History</h3>
      <div class="minmax">Highest Price : Rp <span class="highest"><?php echo number_format($minmax->highest, 0, ",", ".");?></span></div>
      <div class="minmax">Lowest Price : Rp <span class="lowest"><?php echo number_format($minmax->lowest, 0, ",", ".");?></span></div>
      <div id="chartContainer"></div>

      
    </div>
  </div>
  <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/canvasjs.min.js'); ?>"></script>
<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
</script>

<script type="text/javascript">

    $(function () {
        var newDataCount = 0;
        var current_price = 0;
        var lowest_price = 0;
        var highest_price = 0;
        var dataPoints = [];

        var chart = new CanvasJS.Chart("chartContainer", {
          theme: "light2",
          title: {
            text: ""
          },
          data: [{
            type: "line",
            color: "#FFC800",
            xValueType: "dateTime",
            xvalueFormatString: "DD MMM, YYYY HH:mm TT",
            xlabelAngle: -90,
            dataPoints: dataPoints
          }]
        });

        updateData();

        function addData(data) {
          if(data.count){
            newDataCount = newDataCount + data.count;
            $.each(data.data_points, function(key, value) {
              dataPoints.push({x: value.x * 1000, y: parseInt(value.y)});
              if(lowest_price > parseInt(value.y) || lowest_price == 0){
                lowest_price = parseInt(value.y);
              }
              if(highest_price < parseInt(value.y)){
                highest_price = parseInt(value.y);
              }

              if(current_price != parseInt(value.y)){
                current_price = parseInt(value.y);
              }
            });

            $(".highest").html(formatMoney(highest_price));
            $(".lowest").html(formatMoney(lowest_price));
            $(".current_price").html(formatMoney(current_price));
          }
          chart.render();
          setTimeout(updateData, 20000);
        }

        function updateData() {
          $.getJSON("<?php echo base_url();?>get_history/<?php echo $product->product_id;?>/"+newDataCount, addData);
        }

        function formatMoney(x){
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    });
</script>

<?php include("inc_footer.php");?>