<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('All_model');

    }

	public function index()
	{
		$this->load->view('submission_form');
	}

	public function do_submission()
	{
		$link = $this->input->post('link');
		$product_data = $this->retrieve_data($link);


		//GET PRODUCT INFO FROM FABELIO 
		if($product_data){
			//SAVE TO DB
			$this->All_model->save_product($link,$product_data['product_id'],$product_data['product_title'],$product_data['product_price'],$product_data['product_desc'],$product_data['json']);

			$encoded_id = $this->All_model->encode_val($product_data['product_id']);
			$this->session->set_userdata("encoded_id",$encoded_id);
			redirect(base_url()."product_detail");
		}else{
			$this->session->set_flashdata('submission_error', 'Failed to get product information');
		}

		redirect("/");

	}

	public function product_list()
	{
		$data['products'] = $this->All_model->select_template('deleted_at',NULL,'fab_product');
		$this->load->view('product_list',$data);
	}

	public function product_detail()
	{
		$encoded_id = $this->uri->segment('2');
		if($encoded_id==""){
			$encoded_id = $this->session->userdata('encoded_id');
		}

		$this->session->set_userdata("encoded_id",$encoded_id);

		if($encoded_id){
			$product_id = $this->All_model->decode_val($encoded_id);
			$data['product'] = $this->All_model->get_product($product_id);
			$data['history'] = $this->All_model->get_price_history($product_id);
			$data['minmax'] = $this->All_model->get_price_high_low($product_id);
			$this->load->view('product_detail',$data);
		}else{
			redirect('/');
		}
	}

	public function get_history()
	{
		$product_id = $this->uri->segment('2');
		$data_count = $this->uri->segment('3');
		$history = $this->All_model->get_price_history($product_id,$data_count);
		if($history){
			$result['count'] = sizeof($history);
			$result['data_points'] = array();
			foreach($history as $h){
				$data = array();
		        $data['y'] = $h->new_price;
		        $data['x'] = strtotime($h->created_at);
		        $result['data_points'][] = $data;
			}
		}else{
			$result['count'] = 0;
			$result['data_points'] = array();
		}
		echo json_encode($result);
	}

	public function update_price()
	{
		$products = $this->All_model->select_template('deleted_at',NULL,'fab_product');

		if($products){
			foreach($products as $prod){
				$this->All_model->create_scheduler($prod->product_id,$prod->product_link,$prod->product_price);
			}
		}
	}

	public function do_scheduler()
	{
		$tasks = $this->All_model->select_template('finished_at',NULL,'fab_retrieve_scheduler');
		if($tasks){
			foreach($tasks as $t){
				$product_data = $this->retrieve_data($t->product_link);
				if($product_data){
					$this->All_model->save_price_history($t->product_id,$product_data['product_price'],$t->product_price);
					$this->All_model->finish_scheduler($t->id,$product_data['json'],$product_data['product_price']);
				}
			}
		}
	}

	

	public function retrieve_data($link){

		$insider_url = "https://fabelio.com/insider/data/product/id/";

		try {
		    $html = file_get_contents($link);
		} catch (Exception $e) {
		    return false;
		}

		if($html){
			$dom = new DOMDocument();

			$internalErrors = libxml_use_internal_errors(true);
			$dom->loadHTML($html);
			libxml_use_internal_errors($internalErrors);


			$xpath = new DOMXPath($dom);
			
			
			//GET PRODUCT ID
			$rating_div = $xpath->query('//div[@id="product-ratings"]');
			if($rating_div->item(0)!==FALSE){
				$product_id =  $rating_div->item(0)->getAttribute('data-product-id');
			}

			//GET PRODUCT TITLE
			$title_div = $xpath->query('//h1[@class="page-title"]');
			if($title_div->item(0)!==FALSE){
				$product_title =  $title_div->item(0)->textContent;
			}

			//GET PRODUCT DESC
			$desc_div = $xpath->query('//div[@id="description"]');
			if($desc_div->item(0)!==FALSE){
				$product_desc =  $desc_div->item(0)->textContent;
			}


			//GET PRODUCT PRICE
			$price_div = $xpath->query('//div[@class="product-price-'.$product_id.'"]');
			if($price_div->item(0)!==FALSE){
				$children = $xpath->query("descendant::span[@class='price']", $price_div->item(0));
				if($price_div->item(0)!==FALSE){
					$product_price =  $children->item(0)->textContent;
					$search = array("Rp"," ",".");
					$replace = array("","","");
					$product_price = intval(str_replace($search,$replace, $product_price));
				}
			}

			

			//GET PRODUCT INFO FROM FABELIO 
			$json = file_get_contents($insider_url.$product_id);
			if($json){
				$json = json_decode($json,true);
				if($json['product']['id']){
					$data['link'] = $link;
					$data['product_id'] = $product_id;
					$data['product_title'] = $product_title;
					$data['product_price'] = $product_price;
					$data['product_desc'] = $product_desc;
					$data['json'] = $json;

					return $data;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}


	}
}
