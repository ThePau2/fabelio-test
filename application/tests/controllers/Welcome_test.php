<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Welcome_test extends TestCase
{

	public function test_index()
	{
		$output = $this->request('GET', ['Welcome', 'index']);
		$this->assertStringContainsStringIgnoringCase('<title>Fabelio Submission Page</title>', $output,'WRONG');
	}

	public function test_do_submission()
	{
		$output = $this->request(
			'POST',
			['Welcome', 'do_submission'],
			[
				'link' => 'https://fabelio.com/ip/meja-makan-cessi-white-new.html',
			]
		);
		$this->assertRedirect('/product_detail', $output);
	}


	public function test_do_submission_failed()
	{
		$output = $this->request(
			'POST',
			['Welcome', 'do_submission'],
			[
				'link' => 'https://fabelio.com/ip/meja-makan-cesew.html',
			]
		);
		$this->assertRedirect('/', $output);
	}


	public function test_product_list()
	{
		$output = $this->request('GET', ['Welcome', 'product_list']);
		$this->assertStringContainsStringIgnoringCase('<title>Fabelio Product List Page</title>', $output,'WRONG');
	}

}
